var args:[String] = ["xyz", "19500331", "4"]
var da = Int(args[1])!
let YYYY = da/10000
let MM = (da%10000)/100
var DD = da%100
var isLeap = false
var isError = false
var day = 5
var monthGap = [3,1,3,2,3,2,3,3,2,3,2,3]
var month = [31,28,31,30,31,30,31,31,30,31,30,31]
var now = 2016

if(YYYY % 400 == 0 || (YYYY % 4 == 0 && YYYY % 100 != 0)){
    isLeap = true
    month[1] = 29
}else{
    isLeap = false
}

if(DD < 1 || DD > month[MM-1] ){
    isError = true
}
if(MM > 12 || MM < 1){
    isError = true
}


if(isError){
    print("error")
}else{
    for(var i = 2 ; i <= MM ; i++ ){
        day+=monthGap[i-2]
    }
    for(var j = 2 ; j <= DD ; j++){
        day++
    }
    if(YYYY > now){
        repeat {
            if((MM == 2 && DD == 29) || MM > 2 ){
                now++
            }
            if(now % 400 == 0 || (now % 4 == 0 && now % 100 != 0)){
                day += 2
            }else{
                day++
            }   
            if(MM == 1 || (MM == 2 && DD < 29)){
                now++
            }
        } while(YYYY != now)
    }else if (YYYY < now){
        repeat {
            if(MM == 1 || (MM == 2 && DD < 29)){
                now--
            }
            if(now % 400 == 0 || (now % 4 == 0 && now % 100 != 0)){
                day -= 2
            }else{
                day--
            }
            if(day < 7){
                day += 7
            }
            if((MM == 2 && DD == 29) || MM > 2 ){
                now--
            }
        } while(YYYY != now)
    }
    let week = ["日","一","二","三","四","五","六",]
    print("\(YYYY)")
    print("\(MM)")
    print("\(DD)")
    print("星期\(week[day%7])")
    day-=(DD-1)
    day+=35
    DD = 1
    day%=7
    var format = Int(args[2])!
    var loca = format
    for(var w = loca ; w < week.count + loca ; w++){
        print(week[w%7] , terminator : "\t")
    }
    print("")
    if(format > day) {
        day+=7
    }
    for(var d = loca ; d < day  ; d++){
        print("",terminator:"\t")
        loca++
    }
    if(day > 7 ){
        day%=7  
    } 
    for(var d = DD ; d <= month[MM-1] ; d++){
        print(d, terminator : "" )
        print("",terminator:"\t")
        loca++
        if(loca%7 == format ){
            loca=format
            print()
        }
    }
}